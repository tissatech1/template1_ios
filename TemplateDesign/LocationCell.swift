//
//  LocationCell.swift
//  Restaurant
//
//  Created by TISSA Technology on 1/4/21.
//

import UIKit

class LocationCell: UITableViewCell {
    @IBOutlet weak var viewstoreBtn: UIButton!

    @IBOutlet weak var placeLbl: UILabel!
    
    @IBOutlet weak var placeadd: UILabel!
    @IBOutlet weak var placename: UILabel!
    
    @IBOutlet weak var baseouter: UIView!
    
    @IBOutlet weak var outerviewupper: UIView!
    
    @IBOutlet weak var imagerestaurant: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
