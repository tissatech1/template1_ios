//
//  firstTimeLocation.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 3/23/21.
//

import UIKit
import Alamofire
import SDWebImage

class firstTimeLocation: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var locationTable: UITableView!
    var fetchedItems = NSArray()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        adminlogincheck()
        
        locationTable.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "CELL")

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return fetchedItems.count
       
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      
          
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! LocationCell
            
            cell1.selectionStyle = .none
            
        cell1.baseouter.layer.cornerRadius = 10
        cell1.outerviewupper.layer.cornerRadius = 10

        
        cell1.imagerestaurant.layer.masksToBounds = false
        cell1.imagerestaurant.layer.cornerRadius = 10
        cell1.imagerestaurant.clipsToBounds = true
        
        
            cell1.viewstoreBtn.tag = indexPath.row
            cell1.viewstoreBtn.addTarget(self, action:#selector(viewstoreBtnClicked(_:)), for: .touchUpInside)
            
        let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

        let city = (dictObj["city"] as! String)
        let state = (dictObj["state"] as! String)
        
            cell1.placeLbl.text = city + "," + state
            cell1.placename.text = (dictObj["name"] as! String)
            cell1.placeadd.text = (dictObj["address"] as! String)
            
        var urlStr = String()
        if dictObj["restaurant_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["restaurant_url"] as! String

        }


        let url = URL(string: urlStr )

       cell1.imagerestaurant.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell1.imagerestaurant.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell1.imagerestaurant.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell1.imagerestaurant.image = image
            }
        }
        
            return cell1
      
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
           
            return 167
        
    }
    
    @IBAction func viewstoreBtnClicked(_ sender: UIButton) {
    
  //  self.performSegue(withIdentifier: "home", sender: self)

        let dictObj = self.fetchedItems[sender.tag] as! NSDictionary
        
        let restidint = dictObj["restaurant_id"] as? NSNumber
        
        let restid = restidint?.stringValue
        
        GlobalClass.restaurantGlobalid = restid ?? "0"
        
        let defaults = UserDefaults.standard
        
        defaults.set(restid, forKey: "clickedStoreId")
        
        let addressline = (dictObj["address"] as! String)
        let cityline = (dictObj["city"] as! String)
        
        defaults.set(addressline, forKey: "addresslocation")
        defaults.set(cityline, forKey: "citylocation")

        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    
    //MARK: Admin Login

    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                           self.getlocationList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }


    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                    //    ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
   
    
    func getlocationList()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "adminToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/?group_name=madurai"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    
                                self.fetchedItems  = dict.value(forKey: "results") as! NSArray
                                     
                                    print( self.fetchedItems)
                                    
                                    if self.fetchedItems.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No location available")
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        locationTable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                      //  self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
}
